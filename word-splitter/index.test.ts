// splitOnDoubleLetter(‘Letter’) -> [‘let’, ‘ter’]
// splitOnDoubleLetter(‘Really’) -> [‘real’, ‘ly’]
// splitOnDoubleLetter(‘Happy’) -> [‘hap’, ‘py’]
// splitOnDoubleLetter(‘Shall’) -> [‘shal’, ‘l’]
// splitOnDoubleLetter(‘Tool’) -> [‘to’, ‘ol’]
// splitOnDoubleLetter(‘Mississippi’) -> [‘mis’, ‘sis’, ‘sip’, ‘pi’]
// splitOnDoubleLetter(‘Easy’) returns []

import { splitOnDoubleLetter } from ".";

it("should return [let, ter] for Letter", () => {
  // at least 1 assertion
  const wordArray = splitOnDoubleLetter("Letter");
  expect(wordArray).toEqual(["let", "ter"]);
})

it("should return [hap, py] for Happy", () => {
  // at least 1 assertion
  const wordArray = splitOnDoubleLetter("Happy");
  expect(wordArray).toEqual(["hap", "py"]);
})

it("should return [mis, sis, sip, pi] for Mississippi", () => {
  // at least 1 assertion
  const wordArray = splitOnDoubleLetter("Mississippi");
  expect(wordArray).toEqual(["mis", "sis", "sip", "pi"]);
})