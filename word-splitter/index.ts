export function splitOnDoubleLetter(aString) {
  const newArray = [];
  let str = aString.toLowerCase();
  for (let i = 0; i < str.length - 1; i++) {
    if (str[i] === str[i+1]) {
      newArray.push(str.slice(0, i + 1));
      str = str.slice(i + 1);
    }
  }
  newArray.push(str);
  return newArray;
}