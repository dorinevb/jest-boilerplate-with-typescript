export function checkPassword(str) {
  if (str.length < 6 || /\s/.test(str)) {
    return "invalid";
  }
  let strengthCriteria = 0;
  const tips = [];
  if (/[a-z]/.test(str)) {
    strengthCriteria += 1;
  } else {
    tips.push("Has at least 1 lower case character.");
  }
  if (/[A-Z]/.test(str)) {
    strengthCriteria += 1;
  } else {
    tips.push("Has at least 1 upper case character.");
  }
  if (!/^[a-zA-Z0-9]+$/.test(str)) {
    strengthCriteria += 1;
  } else {
    tips.push("Has at least 1 special character.");
  }
  if (str.length >= 8) {
    strengthCriteria += 1;
  } else {
    tips.push("Is at least 8 characters long.");
  }
  if (/[0-9]/.test(str)) {
    strengthCriteria += 1;
  } else {
    tips.push("Has at least 1 digit.");
  }
  if (strengthCriteria <= 2) {
    console.log("Your password is weak, to improve it make sure that it: ");
    console.log(tips);
    return "weak";
  }
  if (strengthCriteria === 3 || strengthCriteria === 4) {
    return "moderate";
  }
  if (strengthCriteria === 5) {
    return "strong";
  }
}