// stonk -> “Invalid”
// pass word -> “Invalid”
// password -> “Weak”
// 11081992 -> “Weak”
// mySecurePass123 -> “Moderate”
// !@!pass1 -> “Moderate”
// @S3cur1ty -> “Strong”


import { checkPassword } from ".";

it("should return invalid for stonk", () => {
  const passwordStrength = checkPassword("stonk");
  expect(passwordStrength).toBe("invalid");
})

it("should return invalid for pass word", () => {
  const passwordStrength = checkPassword("pass word");
  expect(passwordStrength).toBe("invalid");
})

it("should return weak for password", () => {
  const passwordStrength = checkPassword("password");
  expect(passwordStrength).toBe("weak");
})

it("should return weak for 11081992", () => {
  const passwordStrength = checkPassword("11081992");
  expect(passwordStrength).toBe("weak");
})

it("should return moderate for mySecurePass123", () => {
  const passwordStrength = checkPassword("mySecurePass123");
  expect(passwordStrength).toBe("moderate");
})

it("should return moderate for !@!pass1", () => {
  const passwordStrength = checkPassword("!@!pass1");
  expect(passwordStrength).toBe("moderate");
})

it("should return strong for @S3cur1ty", () => {
  const passwordStrength = checkPassword("@S3cur1ty");
  expect(passwordStrength).toBe("strong");
})
