import { wordGame } from ".";

it("should return [[spot, see], []] for See Spot run. See Spot jump. Spot likes jumping. See Spot fly.", () => {
  // at least 1 assertion
  const wordArrays = wordGame("See Spot run. See Spot jump. Spot likes jumping. See Spot fly.");
  expect(wordArrays).toEqual([["spot", "see"], []]);
})

it("should return [[], [test]] for test Test test test% test test.", () => {
  // at least 1 assertion
  const wordArrays = wordGame("test Test test test% test test.");
  expect(wordArrays).toEqual([[], ["test"]]);
})