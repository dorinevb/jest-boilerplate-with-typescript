export function wordGame(str) {
  const lowerStr = str.toLowerCase();
  const cleanStr = lowerStr.replace(/[^\w\'\s]/gi, '');
  const wordArray = cleanStr.split(" ");
  const wordObject = {};
  let acquaintances = [];
  let friends = [];
  for (const word of wordArray) {
    if (!(word in wordObject)) {
      wordObject[word] = 1;
    } else {
      wordObject[word] += 1;
    }
    if (wordObject[word] === 3) {
      acquaintances.push(word);
    }
    if (wordObject[word] === 5) {
      friends.push(word);
      acquaintances = acquaintances.filter(w => w !== word);
    }
  }
  return [acquaintances, friends];
}
