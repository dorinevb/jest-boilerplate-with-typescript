// fiscalCode({
//   name: "Matt",
//   surname: "Edabit",
//   gender: "M",
//   dob: "1/1/1900"
//  }) ➞ "DBTMTT00A01"
//  fiscalCode({
//   name: "Helen",
//   surname: "Yu",
//   gender: "F",
//   dob: "1/12/1950"
//  }) ➞ "YUXHLN50T41"
//  fiscalCode({
//   name: "Mickey",
//   surname: "Mouse",
//   gender: "M",
//   dob: "16/1/1928"
//  }) ➞ "MSOMKY28A16"
 

import { fiscalCode } from ".";

it(`should return DBTMTT00A01 for 
    {
      name: "Matt",
      surname: "Edabit",
      gender: "M",
      dob: "1/1/1900"
    }`, () => {
  // at least 1 assertion
  const fiscalNum = fiscalCode({
    name: "Matt",
    surname: "Edabit",
    gender: "M",
    dob: "1/1/1900"
   });
  expect(fiscalNum).toBe("DBTMTT00A01");
})