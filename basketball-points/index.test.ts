/** 
points(1, 1) ➞ 5
points(7, 5) ➞ 29
points(38, 8) ➞ 100
points(0, 1) ➞ 3
points(0, 0) ➞ 0
*/

import { points } from ".";

it("should add 1 two-pnt and 1 three-punt to equal 5", () => {
  // at least 1 assertion
  const total = points(1, 1);
  expect(total).toBe(5);
})

it("should add 38 two-pnt and 8 three-punt to equal 5", () => {
  // at least 1 assertion
  const total = points(38, 8);
  expect(total).toBe(100);
})

it("should add 0 two-pnt and 0 three-punt to equal 5", () => {
  // at least 1 assertion
  const total = points(0, 0);
  expect(total).toBe(0);
})

it("should throw for negative two-pnt", () => {
  expect(() => points(-1, 1)).toThrow("negative TwoPoints");
})