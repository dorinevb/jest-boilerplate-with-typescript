export function points(twoPointers, threePointers) {
  if (twoPointers < 0) {
    throw new Error("negative TwoPoints");
  }
  return twoPointers * 2 + threePointers * 3;
}