export function humanReadable(num) {
  
  const hours = Math.floor(num/3600);
  const minutes = Math.floor((num % 3600) / 60);
  const seconds = num - hours*3600 - minutes*60;

  function numToString(num) {
    return String(num).padStart(2, '0');
  }

  const hoursString = numToString(hours);
  const minutesString = numToString(minutes);
  const secondsString = numToString(seconds);

  return `${hoursString}:${minutesString}:${secondsString}`
}
