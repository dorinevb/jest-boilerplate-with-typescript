// humanReadable(0) -> '00:00:00'
// humanReadable(59) -> '00:00:59'
// humanReadable(60) -> '00:01:00'
// humanReadable(90) -> '00:01:30'
// humanReadable(3599) -> '00:59:59'
// humanReadable(3600) -> '01:00:00'
// humanReadable(45296) -> '12:34:56'
// humanReadable(86399) -> '23:59:59'
// humanReadable(86400) -> '24:00:00'
// humanReadable(359999) -> '99:59:59

import { humanReadable } from ".";

it("should return 00:00:00 for 0", () => {
  const time = humanReadable(0);
  expect(time).toBe("00:00:00");
})

it("should return 12:34:56 for 45296", () => {
  const time = humanReadable(45296);
  expect(time).toBe("12:34:56");
})

it("should return 99:59:59 for 359999", () => {
  const time = humanReadable(359999);
  expect(time).toBe("99:59:59");
})