export function pigLatinSentence(str) {

  const wordsArray = str.toLowerCase().split(" ");
  const newWordsArray = [];
  let newWord = "";
  let newSentence = "";

  for (const word of wordsArray) {
    let indexVowel = -1;
    for (const ch of word) {
      if (ch.match(/[aeiou]/)) {
        indexVowel = word.indexOf(ch);
        break;
      }
    }
    indexVowel === 0 ? newWord = word + "way" : newWord = word.slice(indexVowel) + word.slice(0, indexVowel) + "ay";
    newWordsArray.push(newWord);
  }
  
  newSentence = newWordsArray.join(" ");
  return newSentence;
}