import { pigLatinSentence } from ".";

it("should return isthay isway igpay atinlay for this is pig latin", () => {
  const pigLatin = pigLatinSentence("this is pig latin");
  expect(pigLatin).toBe("isthay isway igpay atinlay");
})

it("should return allway eetstray ournaljay for wall street journal", () => {
  const pigLatin = pigLatinSentence("wall street journal");
  expect(pigLatin).toBe("allway eetstray ournaljay");
})

it("should return aiseray ethay idgebray for raise the bridge", () => {
  const pigLatin = pigLatinSentence("raise the bridge");
  expect(pigLatin).toBe("aiseray ethay idgebray");
})

it("should return allway igspay oinkway for all pigs oink", () => {
  const pigLatin = pigLatinSentence("all pigs oink");
  expect(pigLatin).toBe("allway igspay oinkway");
})

it("should return xxxay for xxx", () => {
  const pigLatin = pigLatinSentence("xxx");
  expect(pigLatin).toBe("xxxay");
})