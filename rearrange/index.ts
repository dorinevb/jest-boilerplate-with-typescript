export function rearrange(str) {
  if (str.trim().length === 0) {
    return "";
  }
  const sortedArray = ["", "", "", "", "", "", "", "", ""];
  const wordsArray = str.split(" ");
  for (let word of wordsArray) {
    let index = parseInt(word.match(/\d+/)[0]) - 1;
    word = word.replace(/[0-9]/g, "");
    sortedArray[index] = word;
  }
  const sentence = sortedArray.join(" ").trim();
  return sentence;
};