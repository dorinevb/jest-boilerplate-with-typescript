// rearrange("is2 Thi1s T4est 3a") ➞ "This is a Test"
// rearrange("4of Fo1r pe6ople g3ood th5e the2") ➞ "For the good of the
// people"
// rearrange("5weird i2s JavaScri1pt dam4n so3") ➞ "JavaScript is so
// damn weird"
// rearrange(" ") ➞ ""

import { rearrange } from ".";

it("should return This is a Test for is2 Thi1s T4est 3a", () => {
  const sentence = rearrange("is2 Thi1s T4est 3a");
  expect(sentence).toBe("This is a Test");
})

it("should return For the good of  the people for 4of Fo1r pe6ople g3ood th5e the2", () => {
  const sentence = rearrange("4of Fo1r pe6ople g3ood th5e the2");
  expect(sentence).toBe("For the good of the people");
})

it("should return JavaScript is so damn weird for 5weird i2s JavaScri1pt dam4n so3", () => {
  const sentence = rearrange("5weird i2s JavaScri1pt dam4n so3");
  expect(sentence).toBe("JavaScript is so damn weird");
})

it("should return \"\" for \" \"", () => {
  const sentence = rearrange(" ");
  expect(sentence).toBe("");
})