export function elasticize(str) {
  if (str.length < 3) {
    return str;
  }
  let newStr = "";
  let factor = 1;
  // String with uneven length
  if (str.length % 2 === 1) {
    let peak = Math.floor(str.length/2) + 1;
    for (let i = 0; i < peak; i++) {
      newStr += str[i].repeat(factor);
      factor += 1;
    }
    factor -= 2;
    for (let i = peak; i <str.length; i++) {
      newStr += str[i].repeat(factor);
      factor -= 1;
    }
    return newStr;
  // String with even length
  } else {
    let peak = str.length/2;
    for (let i = 0; i < peak; i++) {
      newStr += str[i].repeat(factor);
      factor += 1;
    }
    factor -= 1;
    for (let i = peak; i <str.length; i++) {
      newStr += str[i].repeat(factor);
      factor -= 1;
    }
    return newStr;
  }
}
