// elasticize("ANNA") ➞ "ANNNNA"
// elasticize("KAYAK") ➞ "KAAYYYAAK"
// elasticize("X") ➞ "X"

import { elasticize } from ".";

it("should return ANNNNA for ANNA", () => {
  const elasticWord = elasticize("ANNA");
  expect(elasticWord).toBe("ANNNNA");
})

it("should return KAAYYYAAK for KAYAK", () => {
  const elasticWord = elasticize("KAYAK");
  expect(elasticWord).toBe("KAAYYYAAK");
})

it("should return X for X", () => {
  const elasticWord = elasticize("X");
  expect(elasticWord).toBe("X");
})
