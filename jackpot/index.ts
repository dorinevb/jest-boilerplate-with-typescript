export function checkJackpot(array) {
  const firstElement = array[0];
  for (let i = 0; i < array.length; i++) {
    if (array[i] !== firstElement) {
      return false;
    }
    return true;
  }
}