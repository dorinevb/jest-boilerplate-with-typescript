/**testJackpot(["@", "@", "@", "@"]) ➞ true
testJackpot(["abc", "abc", "abc", "abc"]) ➞ true
testJackpot(["SS", "SS", "SS", "SS"]) ➞ true
testJackpot(["&&", "&", "&&&", "&&&&"]) ➞ false
testJackpot(["SS", "SS", "SS", "Ss"]) ➞ false */

import { checkJackpot } from ".";

it("should return true for an array with only @ strings", () => {
  // at least 1 assertion
  const aBoolean = checkJackpot(["@", "@", "@", "@"]);
  expect(aBoolean).toBe(true);
})

