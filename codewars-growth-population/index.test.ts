import { nbYear } from ".";

it("should return 3 for 1000, 2, 50, 1200", () => {
  const years = nbYear(1000, 2, 50, 1200);
  expect(years).toBe(3);
})