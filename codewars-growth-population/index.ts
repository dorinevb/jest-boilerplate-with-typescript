export function nbYear(p0, percent, aug, p) {
  let currentPopulation = p0;
  let years = 0;
  while (currentPopulation < p) {
    currentPopulation += Math.floor(currentPopulation*(percent/100) + aug);
    years += 1;
  }
  return years;
}