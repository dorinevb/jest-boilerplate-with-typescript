// charCount("a", "edabit") ➞ 1
// charCount("c", "Chamber of secrets") ➞ 1
// charCount("B", "boxes are fun") ➞ 0
// charCount("b", "big fat bubble") ➞ 4
// charCount("e", "javascript is good") ➞ 0
// charCount("!", "!easy!") ➞ 2

import { countCharInString } from ".";

it("should return 1 for a in edabit", () => {
  // at least 1 assertion
  const charCount = countCharInString("a", "edabit");
  expect(charCount).toBe(1);
})

it("should return 1 for c in Chamber of secrets", () => {
  // at least 1 assertion
  const charCount = countCharInString("c", "Chamber of secrets");
  expect(charCount).toBe(1);
})

it("should return 0 for b in Boxes are fun", () => {
  // at least 1 assertion
  const charCount = countCharInString("b", "Boxes are fun");
  expect(charCount).toBe(0);
})

it("should return 4 for b in big fat bubble", () => {
  // at least 1 assertion
  const charCount = countCharInString("b", "big fat bubble");
  expect(charCount).toBe(4);
})

it("should return 0 for e in javascript is good", () => {
  // at least 1 assertion
  const charCount = countCharInString("e", "javascript is good");
  expect(charCount).toBe(0);
})

it("should return 2 for ! in !easy!", () => {
  // at least 1 assertion
  const charCount = countCharInString("!", "!easy!");
  expect(charCount).toBe(2);
})