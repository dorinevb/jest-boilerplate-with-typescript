export function countCharInString(str1, str2) {
  let count = 0;
  str2.split('').forEach(function(char) {
    if (char === str1) {
      count += 1
    }
  });
  return count;
}